﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebBayZon.Startup))]
namespace WebBayZon
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
