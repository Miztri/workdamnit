﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DataLayer;
using EntityFrameworkDB;
using Services;
using WebBayZon.Models;

namespace WebBayZon.Controllers
{
    public class OrdersController : Controller
    {
        private WebBayZonDBContext db = new WebBayZonDBContext();

        ICustomerService service = new CustomerService();

        // GET: Orders
        public ActionResult Index()
        {
            List<CustomerAndTotalOrderCostDto> products = service.GetCustomerAndTotalOrderCosts();
            List<Customer> listCustomers = service.GetAllCustomers();
            List<ProductOrder> customerOrders = new List<ProductOrder>();
            int countof = 0;
            foreach (var productsSold in products)
            {
                ProductOrder setOrder = new ProductOrder()
                {
                    CustomerID = listCustomers[0].CustomerID,
                    FirstName = productsSold.FirstName,
                    LastName = productsSold.LastName,
                    ProductPrices = productsSold.TotalCost
                };
                customerOrders.Add(setOrder);
                countof++;
            };
            return View(customerOrders);
        }

        // GET: Orders/Details/5
        public ActionResult Details(int ID)
        {
            return View();
        }

        // GET: Orders/Create
        public ActionResult Create()
        {
            List<Customer> customers = service.GetAllCustomers();
            List<Product> products = service.GetAllProducts();
            List<SelectListItem> listCustomers = new List<SelectListItem>();
            List<SelectListItem> listProducts = new List<SelectListItem>();

            foreach( var customer in customers)
            {
                listCustomers.Add(new SelectListItem
                {
                    Text = customer.FirstName + " " + customer.LastName,
                    Value = customer.CustomerID.ToString()
                });
            }
            ViewBag.Customers = listCustomers;

            foreach (var product in products)
            {
                listProducts.Add(new SelectListItem
                {
                    Text = product.ProductName + "  £" + product.Price,
                    Value = product.ProductID.ToString()
                });
            }

            ViewBag.Products = new MultiSelectList(listProducts, "Value", "Text", null);

            return View();
        }

        // POST: Orders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(OrderViewModel ovm, FormCollection form)
        {
            List<Product> allProducts = new List<Product>();

            Customer selectedCustomer = service.GetCustomer(Int32.Parse(ovm.CustomerID));

            foreach (string prodIDinString in ovm.ProductIDs)
            {
                Product productToAdd = service.GetProduct(Int32.Parse(prodIDinString));
                allProducts.Add(productToAdd);
            }

            Order setOrder = new Order()
            {
                Customer = selectedCustomer,
                Products = allProducts,
                CustomerID = selectedCustomer.CustomerID

            };
            service.AddOrder(setOrder);
            return RedirectToAction("Index");
            //try
            //{
                
            //}
            //catch
            //{
            //    return RedirectToAction("Index");
            //}
        }

            // GET: Orders/Edit/5
            public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            ViewBag.CustomerID = new SelectList(db.Customers, "CustomerID", "FirstName", order.CustomerID);
            ViewBag.ProductID = new SelectList(db.Products, "ProductID", "ProductName", order.Products);
            return View(order);
        }

        // POST: Orders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "OrderID,CustomerID,ProductID")] Order order)
        {
            if (ModelState.IsValid)
            {
                db.Entry(order).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CustomerID = new SelectList(db.Customers, "CustomerID", "FirstName", order.CustomerID);
            //ViewBag.ProductID = new SelectList(db.Products, "ProductID", "ProductName", order.ProductID);
            return View(order);
        }

        // GET: Orders/Delete/5
        public ActionResult Delete(int id)
        {
            Order orderDetails;
            orderDetails = service.GetOrder(id);

            List<Product> products = service.GetOrderByCustomer(id);
            Customer name = service.GetCustomer(orderDetails.CustomerID);

            Order setOrder = new Order()
            {
                OrderID = orderDetails.OrderID,
                CustomerID = orderDetails.CustomerID,
                Products = products

            };
            ViewBag.Message = name.FirstName + ", ";
            return View(setOrder);
        }

        // POST: Orders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            service.DeleteOrderByID(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
