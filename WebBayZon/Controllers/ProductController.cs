﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EntityFrameworkDB;
using Services;


namespace WebBayZon.Controllers
{
    public class ProductController : Controller
    {
        ICustomerService service = new CustomerService();
        // GET: Product
        public ActionResult Index()
        {
            List<Product> ProductList = service.GetAllProducts();
            return View(ProductList);
        }

        // GET: Product/Details/5
        public ActionResult Details(int id)
        {
            Product foundProduct;
            foundProduct = service.GetProduct(id);
            return View(foundProduct);
        }
        [HttpGet]
        // GET: Product/Create
        public ActionResult Create()
        {
            return View();
        }
        
        // POST: Product/Create
        [HttpPost]
        public ActionResult Create(Product product)
        {               
          
            service.AddProduct(product);                    
            return RedirectToAction("Index");
            // TODO: Add insert logic here           
           
        }

        // GET: Product/Edit/5
        public ActionResult Edit(int id)
        {
            Product foundproduct;
            foundproduct = service.GetProduct(id);
            return View(foundproduct);
        }

        // POST: Product/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Product product)
        {
            try
            {
                // TODO: Add update logic here
                service.UpDateProduct(product);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Product/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Product/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
