﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EntityFrameworkDB;
using Services;
using DataLayer;

namespace WebBayZon.Controllers
{
    public class CustomerController : Controller
    {
        ICustomerService service = new CustomerService();
        // GET: Customer
        public ActionResult Index()
        {
            List<Customer> CustomerList = service.GetAllCustomers();
            return View(CustomerList);
        }

        // GET: Customer/Details/5
        public ActionResult Details(int id)
        {
            Customer foundCustomer;
            foundCustomer = service.GetCustomer(id);
            return View(foundCustomer);
        }

        // GET: Customer/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public ActionResult Create(Customer customer)
        {
            try
            {
                // TODO: Add insert logic here
                service.AddCustomer(customer);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Customer/Edit/5
        public ActionResult Edit(int id)
        {
            Customer foundCustomer;
            foundCustomer = service.GetCustomer(id);
            return View(foundCustomer);
        }

        // POST: Customer/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Customer customer)
        {
            try
            {
                // TODO: Add update logic here
                service.UpDateCustomer(customer);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Customer/Delete/5
        public ActionResult Delete(int id)
        {
            Customer foundCustomer;
            foundCustomer = service.GetCustomer(id);
            return View(foundCustomer);
        }

        // POST: Customer/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, Customer customer)
        {
            try
            {
                // TODO: Add delete logic here
                customer.CustomerID = id;
                service.DeleteCustomerByID(customer);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
