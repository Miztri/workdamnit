﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebBayZon.Models
{
    public class ProductOrder
    {
        public int CustomerID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public decimal ProductPrices { get; set; }

    }
}