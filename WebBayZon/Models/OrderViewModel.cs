﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EntityFrameworkDB;
using DataLayer;

namespace WebBayZon.Models
{
    public class OrderViewModel
    {
        public string CustomerID { get; set; }
        public int OrderID { get; set; }
        public string[] ProductIDs { get; set; }
    }
}