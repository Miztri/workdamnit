﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using DataLayer;
using Services;
using System.Data.Common;
using EntityFrameworkDB;

namespace WebBayZon.Tests
{
    [TestFixture]
    class WebBayZonTests
    {
        private WebBayZonDBContext dbBefore;
        private WebBayZonDBContext dbAfter;
        private CustomerService customerService;

        [SetUp]
        public void SetUp()
        {
            DbConnection connection = Effort.DbConnectionFactory.CreateTransient();
            dbBefore = new WebBayZonDBContext(connection);
            dbAfter = new WebBayZonDBContext(connection);
            WebBayZonDBContext dbServiece = new WebBayZonDBContext(connection);
            customerService = new CustomerService(dbServiece);
        }

        [Test]
        public void CreatingACustomer()        
        {
            // Arrange
            int CustomerID = customerService.AddCustomer(new Customer
            {
                FirstName = "Sean",
                LastName = "Harrison"
            });
            // Act
            Customer sean = customerService.GetCustomer(CustomerID);
            // Assert
            Assert.That(sean, Is.Not.Null);
            Assert.That(sean.FirstName, Is.EqualTo("Sean"));
            Assert.That(sean.LastName, Is.EqualTo("Harrison"));


            //Act II
            sean.County = "Flintshire";
            customerService.UpDateCustomer(sean);
            Customer sean1 = customerService.GetCustomer(CustomerID);
            //More Assert
            Assert.That(sean1.County, Is.EqualTo("Flintshire"));
        }
       
        
            
                
        
       
            
    }
}
