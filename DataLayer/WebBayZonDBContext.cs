﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityFrameworkDB;
using System.Data.Entity;
using System.Data.Common;

namespace DataLayer
{
    public class WebBayZonDBContext : DbContext
    {
        public WebBayZonDBContext() : base("WebBayZonContext")
        {

        }

        public WebBayZonDBContext(DbConnection connection) : base(connection, true)
        {

        }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Product> Products { get; set; }
    }
}
