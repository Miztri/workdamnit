namespace DataLayer.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using EntityFrameworkDB;

    internal sealed class Configuration : DbMigrationsConfiguration<DataLayer.WebBayZonDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DataLayer.WebBayZonDBContext context)
        {
            context.Customers.RemoveRange(context.Customers);           
            context.Products.RemoveRange(context.Products);
            context.Database.ExecuteSqlCommand("DBCC CHECKIDENT ('Customers', RESEED , 0)");            
            context.Database.ExecuteSqlCommand("DBCC CHECKIDENT ('Products', RESEED , 0)");

            Customer sean = new Customer()
            {
                FirstName = "Sean",
                LastName = "Harrison"
            };            
            Product TV = new Product()
            {
                ProductName = "TV",
                ProductDementions = "55inch",
                Price = 999,
                ProductDescription = "Big",
                ProductInStock = 7,
                InStock = true,
                ProductLocation = "Leeds",
                SellerID = 1
            };

            context.Customers.Add(sean);           
            context.Products.Add(TV);
            context.SaveChanges();
        }
    }
}
