﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityFrameworkDB;
using DataLayer;
using System.ComponentModel;

namespace Services
{
    public class CustomerService : ICustomerService
    {
        WebBayZonDBContext context;

        public CustomerService(WebBayZonDBContext newContext)
        {
            context = newContext;
        }

        public CustomerService()
        {
            context = new WebBayZonDBContext();
        }

        public object[] CustomerID { get; private set; }
        public int orderID { get; private set; }

        public int AddCustomer(Customer customer)
        {
            context.Customers.Add(customer);
            context.SaveChanges();
            return (customer.CustomerID);
        }

        public void AddProduct(Product product)
        {
            context.Products.Add(product);
            context.SaveChanges();
        }
        public int AddOrder(Order order)
        {
            context.Orders.Add(order);
            context.SaveChanges();
            return order.OrderID;
        }
        public void DeleteOrderByID(int orderID)
        {
            Order ordername = context.Orders.Find(orderID);

            context.Orders.Remove(ordername);
            context.SaveChanges();
        }

        public void DeleteCustomerByID(Customer customer)
        {
            Customer customerName = context.Customers.Find(customer.CustomerID);
            if (customerName != null)
            {
                context.Customers.Remove(customerName);
                context.SaveChanges();

            }
        }

        public void DeleteProductByID(int ProductID)
        {
            throw new NotImplementedException();
        }

        public List<Customer> GetAllCustomers()
        {
            List<Customer> customers;
            customers = context.Customers.ToList();
            return customers;
        }

        public List<Product> GetAllProducts()
        {
            List<Product> products;
            products = context.Products.ToList();
            return products;
        }

        public Customer GetCustomer(int CustomerID)
        {
            Customer customer;
            customer = context.Customers.Find(CustomerID);
            return customer;
        }

        public List<CustomerAndTotalOrderCostDto> GetCustomerAndTotalOrderCosts()
        {
            
            List<CustomerAndTotalOrderCostDto> customers = new List<CustomerAndTotalOrderCostDto>();
            List<Order> allOrders = GetAllOrders();
            foreach (Order order in allOrders)
            {
                customers.Add(new CustomerAndTotalOrderCostDto
                {
                    FirstName = order.Customer.FirstName,
                    LastName = order.Customer.LastName,
                    OrderID = order.OrderID,
                    TotalCost = order.Products.Select(p => p.Price).Sum()
                });
            }
            return customers;
           
        }
        public List<Product> GetOrderByCustomer(int orderID)
        {
            List<Product> ordersAre;

            using (WebBayZonDBContext context = new WebBayZonDBContext())
            {
                ordersAre = context.Orders.Where(o => o.OrderID == orderID).SelectMany(o => o.Products).ToList();
            }
            return ordersAre;
        }

        public List<Order> GetAllOrders()
        {
            List<Order> Orders;
            Orders = context.Orders.ToList();
            return Orders;
        }
        public Order GetOrder(int OrderID)
        {
            Order Orders;
            Orders = context.Orders.Find(OrderID);
            return Orders;
        }

        public Product GetProduct(int ProductID)
        {
            Product product;
            product = context.Products.Find(ProductID);
            return product;
        }

        /*public void UpDateCustomer(int CustomerID, Customer customer)
        {

            context.Customers.Single(d => d.CustomerID == CustomerID).FirstName = customer.FirstName;
            context.Customers.Single(d => d.CustomerID == CustomerID).LastName = customer.LastName;
            context.Customers.Single(d => d.CustomerID == CustomerID).AddressLine1 = customer.AddressLine1;
            context.Customers.Single(d => d.CustomerID == CustomerID).AddressLine2 = customer.AddressLine2;
            context.Customers.Single(d => d.CustomerID == CustomerID).City = customer.City;
            context.Customers.Single(d => d.CustomerID == CustomerID).County = customer.County;
            context.Customers.Single(d => d.CustomerID == CustomerID).Postcode = customer.Postcode;
            context.Customers.Single(d => d.CustomerID == CustomerID).HouseNumber_Name = customer.HouseNumber_Name;
            
            context.SaveChanges();
        }*/

        public void UpDateProduct(Product product)
        {
            var productIs = context.Products.Find(product.ProductID);
            productIs.ProductID = product.ProductID;
            productIs.ProductDescription = product.ProductDescription;
            productIs.Price = product.Price;
            productIs.ProductDementions = product.ProductDementions;
            productIs.ProductInStock = product.ProductInStock;
            productIs.ProductLocation = product.ProductLocation;
            productIs.SellerID = product.SellerID;
            productIs.InStock = product.InStock;
            context.SaveChanges();
        }

        public void UpDateCustomer(Customer customer)
        {
            var customerIs = context.Customers.Find(customer.CustomerID);
            customerIs.CustomerID = customer.CustomerID;
            customerIs.FirstName = customer.FirstName;
            customerIs.LastName = customer.LastName;
            customerIs.HouseNumber_Name = customer.HouseNumber_Name;
            customerIs.AddressLine1 = customer.AddressLine1;
            customerIs.AddressLine2 = customer.AddressLine2;
            customerIs.City = customer.City;
            customerIs.County = customer.County;
            customerIs.Postcode = customer.Postcode;
            context.SaveChanges();
        }
    }
}
