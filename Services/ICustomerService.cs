﻿using System.Collections.Generic;
using EntityFrameworkDB;

namespace Services
{
    public interface ICustomerService
    {
        object[] CustomerID { get; }
        int orderID { get; }

        int AddCustomer(Customer customer);
        void AddProduct(Product product);
        int AddOrder(Order order);
        void DeleteCustomerByID(Customer customer);
        void DeleteProductByID(int ProductID);
        void DeleteOrderByID(int orderID);
        List<Customer> GetAllCustomers();
        List<Order> GetAllOrders();
        List<Product> GetAllProducts();
        List<Product> GetOrderByCustomer(int orderID);
        Customer GetCustomer(int CustomerID);
        List<CustomerAndTotalOrderCostDto> GetCustomerAndTotalOrderCosts();
        Product GetProduct(int ProductID);

        Order GetOrder(int OrderID);
        void UpDateCustomer(Customer customer);
        void UpDateProduct(Product product);
        //void UpDateCustomer(Customer customer);
    }
}